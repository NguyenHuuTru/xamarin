﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
	public class DeadLine
	{
		[PrimaryKey, AutoIncrement]
		public int DLID { get; set; }
		public string DLName { get; set; }
		public string DLNotes { get; set; }
		public bool DLDone { get; set; }
		public DateTime DLTime { get; set; }
	}
}
