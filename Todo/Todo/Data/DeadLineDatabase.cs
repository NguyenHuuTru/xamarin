﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;

namespace Todo.Data
{
	public class DeadLineDatabase
	{
        static SQLiteAsyncConnection Database;

        public static readonly AsyncLazy<DeadLineDatabase> Instance = new AsyncLazy<DeadLineDatabase>(async () =>
        {
            var instance = new DeadLineDatabase();
            CreateTableResult result = await Database.CreateTableAsync<DeadLine>();
            return instance;
        });

        public DeadLineDatabase()
        {
            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        }

        public Task<List<DeadLine>> GetItemsAsync()
        {
            return Database.Table<DeadLine>().ToListAsync();
        }

        public Task<List<DeadLine>> GetItemsNotDoneAsync()
        {
            return Database.QueryAsync<DeadLine>("SELECT * FROM [DeadLine] WHERE [Done] = 0");
        }

        public Task<DeadLine> GetItemAsync(int id)
        {
            return Database.Table<DeadLine>().Where(i => i.DLID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(DeadLine item)
        {
            if (item.DLID != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(DeadLine item)
        {
            return Database.DeleteAsync(item);
        }
    }
}
