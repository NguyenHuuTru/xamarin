﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Data;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeadLineIteamPage : ContentPage
	{
		public DeadLineIteamPage()
		{
			InitializeComponent();
		}

        async void OnSaveClicked(object sender, EventArgs e)
        {
            var deadLineItem = (DeadLine)BindingContext;
            DeadLineDatabase database = await DeadLineDatabase.Instance;
            await database.SaveItemAsync(deadLineItem);
            await Navigation.PopAsync();
        }

        async void OnDeleteClicked(object sender, EventArgs e)
        {
            var deadLineItem = (DeadLine)BindingContext;
            DeadLineDatabase database = await DeadLineDatabase.Instance;
            await database.DeleteItemAsync(deadLineItem);
            await Navigation.PopAsync();
        }

        async void OnCancelClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        //void OnDateSelected(object sender, DateChangedEventArgs args)
        //{
        //    Recalculate();    
        //}

        //void Recalculate()
        //{
        //    TimeSpan timeSpan = endDatePicker.Date - DateTime.Now +
        //        (includeSwitch.IsToggled ? TimeSpan.FromDays(1) : TimeSpan.Zero);

        //    resultLabel.Text = String.Format("{0} day{1} between dates",
        //                                        timeSpan.Days, timeSpan.Days == 1 ? "" : "s");
        //}
    }
}