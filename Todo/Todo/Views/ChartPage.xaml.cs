﻿using Microcharts;
using SkiaSharp;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Data;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.ChartEntry;

namespace Todo.Views
{
	
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChartPage : ContentPage
	{
		
		static List<string> dateOfWeek = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
		static Entry[] entries = new Entry[7];
 
		public ChartPage()
		{
			
			InitializeComponent();
			
		}
		protected override async void OnAppearing()
		{
			
			base.OnAppearing();

			TodoItemDatabase database = await TodoItemDatabase.Instance;
			listView.ItemsSource = await database.GetItemsDoneAsync();
			List<TodoItem> todoItems = (List<TodoItem>)listView.ItemsSource;
			
			DateTime Mon = MonDay(DateTime.Now);
			DrawChart(todoItems, Mon);
			Chart.Chart = new BarChart
			{
				Entries = entries,
				LabelOrientation = Orientation.Horizontal,
				ValueLabelOrientation = Orientation.Horizontal,
				LabelTextSize = 20,
			};
		}
		
		

		async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem != null)
			{
				await Navigation.PushAsync(new TodoItemPage
				{
					BindingContext = e.SelectedItem as TodoItem
				});
			}
		}

		static DateTime MonDay(DateTime now)
		{
			DateTime temp = now;
			temp = temp.AddDays(-dateOfWeek.IndexOf(temp.DayOfWeek.ToString()));

			return temp;
		}

		

		 void DrawChart(List<TodoItem> todoItems , DateTime monday  )
        {

			
			for(int i = 0; i< 7; i++)
            {
				int temp = 0;
				foreach(var data in todoItems)
                {
					if(data.Date.Date == monday.AddDays(i).Date)
                    {
						temp++;
                    }
                }
				var entry = new Entry(temp)
				{
					Label = monday.AddDays(i).DayOfWeek.ToString(),
					ValueLabel = temp.ToString(),
					Color = SKColor.Parse("#0b9360")

				};
				entries[i]=entry;
				
            }			
        }
		

	}
}